//! Return vec of all projects inside project paths specified in config
//!
//! See [`list_projects`]

use std::path::PathBuf;

use crate::entity::{hash_project_path, ProjectPathHash};
use crate::gateway::_trait::Config;
use vfs::FileSystem;

/// Glob import for [`list_projects`] use case
pub mod prelude {
    pub use super::{
        list_projects, Error as ListProjectsError, ProjectReference,
        RequestModel as ListProjectsRequestModel,
        ResponseModel as ListProjectsResponseModel,
        Result as ListProjectsResult,
    };
}

/// Parameters needed for [`list_projects`] use case
pub struct RequestModel<'a, FS: FileSystem, G: Config> {
    /// Which file system is used (can be mocked for testing)
    pub file_system: &'a FS,

    /// Which impl of [`Config`] is used (can change gateway plug-in)
    pub gateway: &'a mut G,
}

/// Results of [`list_projects`] use case when successful
#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct ResponseModel {
    /// [`PathBuf`] and [`ProjectPathHash`] for each project
    pub reference_vec: Vec<ProjectReference>,
}

/// [`PathBuf`] and [`ProjectPathHash`] referencing a specific project
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ProjectReference {
    /// Path to project
    pub file_path: PathBuf,

    /// Hash of project path
    pub hash: ProjectPathHash,
}

impl PartialOrd for ProjectReference {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.file_path.partial_cmp(&other.file_path)
    }
}

impl Ord for ProjectReference {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.file_path.cmp(&other.file_path)
    }
}

impl ProjectReference {
    fn from(value: PathBuf) -> Self {
        ProjectReference {
            hash: hash_project_path(&value),
            file_path: value,
        }
    }
}

/// Result of [`list_projects`] use case when unsuccessful
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Error {
    /// Project path specified in config is not accessible, or doesn't exist
    UnableToReadDir(PathBuf),
}

/// Result of [`list_projects`] use case
pub type Result = std::result::Result<ResponseModel, Error>;

impl From<Error> for Result {
    fn from(e: Error) -> Self {
        Err(e)
    }
}

/// Return vec of all projects inside project paths specified in config
///
/// # Example
///
/// ```
/// # use kanban_launcher_lib::entity::ProjectPathHash;
/// # use kanban_launcher_lib::gateway::buffered::{Config, ConfigBuffer};
/// # use kanban_launcher_lib::list_projects_use_case::prelude::*;
/// # use std::path::PathBuf;
/// # use vfs::{MemoryFS, FileSystem};
/// # let mock_file_system = MemoryFS::new();
/// # [
/// #     "/Projects",
/// #     "/Projects/Test1",
/// #     "/Projects/Test2",
/// #     "/Projects/Test3",
/// # ]
/// # .into_iter()
/// # .for_each(|p| mock_file_system.create_dir(p).unwrap());
/// # let buffer = ConfigBuffer {
/// #     project_directory: Some(vec![PathBuf::from("/Projects")]),
/// #     match_pattern: None,
/// # };
/// # let mut mock_gateway = Config(buffer);
/// // GIVEN
/// let request_model = ListProjectsRequestModel {
///     file_system: &mock_file_system,
///     gateway: &mut mock_gateway,
/// };
///
/// // WHEN
/// let result = list_projects(request_model);
///
/// // THEN
/// let mut reference_vec = result.ok().unwrap().reference_vec;
/// reference_vec.sort(); // Sorting needed for consistent test results only
///
/// assert_eq!(
///     reference_vec,
///     vec![
///         ProjectReference {
///             file_path: PathBuf::from("/Projects/Test1"),
///             hash: ProjectPathHash::try_new([7, 8, 14, 9, 15, 14, 13,])
///                 .unwrap()
///         },
///         ProjectReference {
///             file_path: PathBuf::from("/Projects/Test2"),
///             hash: ProjectPathHash::try_new([8, 15, 0, 4, 8, 14, 0,])
///                 .unwrap()
///         },
///         ProjectReference {
///             file_path: PathBuf::from("/Projects/Test3"),
///             hash: ProjectPathHash::try_new([12, 8, 4, 5, 4, 4, 8,])
///                 .unwrap()
///         },
///     ]
/// );
/// ```
///
/// # Errors
///
/// ```
/// # use kanban_launcher_lib::entity::ProjectPathHash;
/// # use kanban_launcher_lib::gateway::buffered::{Config, ConfigBuffer};
/// # use kanban_launcher_lib::list_projects_use_case::prelude::*;
/// # use std::path::PathBuf;
/// # use vfs::{MemoryFS, FileSystem};
/// # let mock_file_system = MemoryFS::new();
/// # let mut buffer = ConfigBuffer {
/// #     project_directory: Some(vec![PathBuf::from("/Projects")]),
/// #     match_pattern: None,
/// # };
/// # let mut mock_gateway = Config(buffer);
/// // GIVEN
/// let request_model = ListProjectsRequestModel {
///     file_system: &mock_file_system,
///     gateway: &mut mock_gateway,
/// };
///
/// // WHEN
/// let result = list_projects(request_model);
///
/// // THEN
/// assert_eq!(
///     result,
///     Err(ListProjectsError::UnableToReadDir(PathBuf::from("/Projects")))
/// );
/// ```
pub fn list_projects<FS: FileSystem, G: Config>(
    RequestModel {
        file_system,
        gateway,
    }: RequestModel<FS, G>,
) -> Result {
    let Some(project_directory_vec) = gateway.read_project_directory_vec() else {
        return Ok(ResponseModel::default());
    };

    let mut dir_vec = Vec::new();

    for path in project_directory_vec {
        let path_string = path.as_os_str().to_str().unwrap_or("");
        match file_system.read_dir(
            path.as_os_str()
                .to_str()
                .expect("1670371448 - Unknown Error"),
        ) {
            Ok(dir) => dir.for_each(|v| {
                dir_vec.push(PathBuf::from(format!("{path_string}/{v}")));
            }),
            Err(_) => return Error::UnableToReadDir(path).into(),
        }
    }

    Ok(ResponseModel {
        reference_vec: dir_vec
            .into_iter()
            .map(ProjectReference::from)
            .collect(),
    })
}
