# kanban_launcher_lib

Launch project based on hash of file path

## Goals

See [`GOALS.md`](./GOALS.md)

## Release Checklist

See [`RELEASE_CHECKLIST.md`](./RELEASE_CHECKLIST.md)

## Version History

See [`VERSION_HISTORY.md`](./VERSION_HISTORY.md)

License: MIT
