//! Data source that returns nothing, used when persistent storage location empty

mod config;

pub use self::config::{Buffer as ConfigBuffer, Config};
