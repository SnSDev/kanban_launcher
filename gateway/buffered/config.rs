use crate::entity::LaunchPattern;
use crate::gateway::_trait::Config as ConfigTrait;
use std::path::PathBuf;

/// An wrapper for [`Buffer`] which implements [`ConfigTrait`]
#[derive(Debug, Default)]
pub struct Config(pub Buffer);

impl ConfigTrait for Config {
    fn read_project_directory_vec(
        &mut self,
    ) -> Option<Vec<std::path::PathBuf>> {
        self.0.project_directory.clone()
    }

    fn read_match_pattern_array(&mut self) -> Option<Vec<LaunchPattern>> {
        self.0.match_pattern.clone()
    }
}

/// Container for buffered data to supply [`ConfigTrait`]
#[derive(
    Debug, Default, PartialEq, Eq, Clone, serde::Serialize, serde::Deserialize,
)]
pub struct Buffer {
    /// Directories that projects are stored in
    pub project_directory: Option<Vec<PathBuf>>,

    /// Patterns used to determine how to launch projects
    pub match_pattern: Option<Vec<LaunchPattern>>,
}
