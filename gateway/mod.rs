//! External persistent data source abstraction
//!
//! Each `gateway` is a trait containing a function for each data action needed
//! for a perspective data set.
//!
//! For example, if you have counter you may have:
//!
//! // TODO: Debug
//! ```text
//! trait Counter {
//!   type E: std::error::Error;
//!   fn current(&mut self) -> Result<u8, E>;
//!   fn add(&mut self, number: u8) -> Result<(), E>;
//!   fn reset(&mut self) -> Result<(), E>;
//! }
//! ```
//!
//! Then if you choose to store the counter's value in ram you could
//! make the following struct in `crate::gateway::ram::Counter`
//!
//! // TODO: Debug
//! ```text
//! # trait CounterTrait {
//! #   type E: std::error::Error;
//! #   fn current(&mut self) -> Result<u8, E>;
//! #   fn add(&mut self, number: u8) -> Result<(), E>;
//! #   fn reset(&mut self) -> Result<(), E>;
//! # }
//! struct Counter {
//!     value: u8,
//! }
//!
//! impl CounterTrait for Counter {
//!     type E = String;
//!
//!     fn current(&mut self) -> Result<u8 E> {
//!         Ok(self.value)
//!     }
//!
//!     fn add(&mut self, number: u8) -> Result<(), E> {
//!         self.value += number;
//!         Ok(())
//!     }
//!
//!     fn reset(&mut self) -> Result<() , E> {
//!         Err("Counter not resettable".to_owned())
//!     }
//! }
//! ```
//!
//! If later you need the counter to persist inside a json file, you could
//! create a struct inside `crate::gateway::json`.  Such a struct may implement
//! many different traits.

pub mod _trait;
pub mod buffered;
