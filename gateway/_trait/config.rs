use crate::entity::LaunchPattern;
use std::path::PathBuf;

/// The required configuration data functions
pub trait Config {
    /// Return vector of directories that projects are stored in
    fn read_project_directory_vec(&mut self) -> Option<Vec<PathBuf>>;

    /// Return vector of patterns used to determine how to launch projects
    fn read_match_pattern_array(&mut self) -> Option<Vec<LaunchPattern>>;
}
