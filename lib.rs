#![warn(missing_docs)]
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

//! Launch project based on hash of file path
//!
//! # Goals
//!
//! See [`GOALS.md`](./GOALS.md)
//!
//! # Release Checklist
//!
//! See [`RELEASE_CHECKLIST.md`](./RELEASE_CHECKLIST.md)
//!
//! # Version History
//!
//! See [`VERSION_HISTORY.md`](./VERSION_HISTORY.md)

pub mod data_object;
pub mod entity;
pub mod gateway;
pub mod hash_dir_use_case;
pub mod list_projects_use_case;
pub mod match_launcher_use_case;
pub mod serde;
