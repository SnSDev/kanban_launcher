//! Return the command to run for the supplied project path
//!
//! See [`match_launcher`]

use crate::{
    data_object::LaunchPathData, entity::LaunchInstructions,
    gateway::_trait::Config as ConfigTrait,
};
use std::path::{Path, PathBuf};
use vfs::FileSystem;

/// Glob import for [`match_launcher`] use case
pub mod prelude {
    pub use super::{
        match_launcher, Error as MatchLauncherError,
        RequestModel as MatchLauncherRequestModel,
        ResponseModel as MatchLauncherResponseModel,
        Result as MatchLauncherResult,
    };
}

/// Parameters needed for [`match_launcher`] use case
pub struct RequestModel<'a, FS: FileSystem, G: ConfigTrait> {
    /// Which file system is used (can be mocked for testing)
    pub file_system: &'a FS,

    /// Which impl of [`ConfigTrait`] is used (can change gateway plug-in)
    pub gateway: &'a mut G,

    /// The path to the project
    pub project_path: &'a Path,
}

/// Results of [`match_launcher`] use case when successful
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ResponseModel {
    /// The command to be run for the specified project path
    pub instructions: LaunchInstructions,

    /// Path data for a specified project
    pub path_data: LaunchPathData,
}

/// Result of [`match_launcher`] use case when unsuccessful
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Error {
    /// The specified directory is invalid or unable to be read
    UnableToReadDirectory,

    /// The specified directory does not contain a recognized project
    NoMatchesFound(PathBuf),
}

/// Result of [`match_launcher`] use case
pub type Result = std::result::Result<ResponseModel, Error>;

impl From<Error> for Result {
    fn from(value: Error) -> Self {
        Err(value)
    }
}

impl From<ResponseModel> for Result {
    fn from(value: ResponseModel) -> Self {
        Ok(value)
    }
}

/// Return the command to run for the supplied project path
///
///
/// # Example
///
/// ```
/// # use kanban_launcher_lib::data_object::LaunchPathData;
/// # use kanban_launcher_lib::entity::{
/// #     LaunchInstructions, LaunchInstructionsToken as Token, LaunchPattern,
/// # };
/// # use kanban_launcher_lib::gateway::{
/// #     buffered::Config, buffered::ConfigBuffer
/// # };
/// # use kanban_launcher_lib::match_launcher_use_case::prelude::*;
/// # use pretty_assertions::assert_eq;
/// # use std::path::PathBuf;
/// # use vfs::{FileSystem, MemoryFS};
/// # const PROJECT_DIRECTORY: &str = "/test";
/// # const FILE_NAME: &str = "B.2";
/// # fn full_path<'a>() -> String {
/// #     format!("{PROJECT_DIRECTORY}/{FILE_NAME}")
/// # }
/// # fn launch_instructions() -> LaunchInstructions {
/// #     vec![
/// #         Token::Application("2".to_owned()),
/// #         Token::Argument("-f".to_owned()),
/// #         Token::FileName,
/// #         Token::Argument("-d".to_owned()),
/// #         Token::ProjectDirectory,
/// #     ]
/// #     .into()
/// # }
/// # fn launch_path_data() -> LaunchPathData {
/// #     LaunchPathData {
/// #         project_directory: PathBuf::from(PROJECT_DIRECTORY),
/// #         file_name: PathBuf::from(FILE_NAME),
/// #         full_path: PathBuf::from(full_path()),
/// #     }
/// # }
/// #
/// # let mock_file_system = MemoryFS::default();
/// # mock_file_system.create_dir(PROJECT_DIRECTORY).unwrap();
/// # mock_file_system.create_dir("/other").unwrap();
/// # let mut writer = mock_file_system.create_file("/test/A.3").unwrap();
/// # writer.flush().unwrap();
/// # let mut writer = mock_file_system.create_file(&full_path()).unwrap();
/// # writer.flush().unwrap();
/// // GIVEN
/// let mut mock_gateway = Config(ConfigBuffer {
///     project_directory: None,
///     match_pattern: Some(vec![
/// #         LaunchPattern {
/// #             launch_instructions: vec![
/// #                 Token::Application("1".to_owned()),
/// #                 Token::FileName,
/// #             ]
/// #             .into(),
/// #             match_pattern: regex::Regex::new(r#".*\.1"#)
/// #                 .unwrap()
/// #                 .into(),
/// #         },
///         LaunchPattern {
///             launch_instructions: launch_instructions(),
///             match_pattern: regex::Regex::new(r#".*\.2"#)
///                 .unwrap()
///                 .into(),
///         },
///         //      ...
/// #         LaunchPattern {
/// #             launch_instructions: vec![
/// #                 Token::Application("3".to_owned()),
/// #                 Token::FileName,
/// #             ]
/// #             .into(),
/// #             match_pattern: regex::Regex::new(r#".*\.3"#)
/// #                 .unwrap()
/// #                 .into(),
/// #         },
///     ]),
/// });
///
/// // WHEN
/// let actual = match_launcher(MatchLauncherRequestModel {
///     file_system: &mock_file_system,
///     gateway: &mut mock_gateway,
///     project_path: &PathBuf::from(PROJECT_DIRECTORY),
/// });
///
/// // THEN
/// assert_eq!(
///     actual,
///     Ok(MatchLauncherResponseModel {
///         instructions: launch_instructions(),
///         path_data: launch_path_data(),
///     }),
/// );
/// ```
///
/// # Errors
///
/// If an invalid or unreadable directory path is specified the
/// [`Error::UnableToReadDirectory`] variant is returned.
///
/// If none of the [`crate::entity::LaunchPattern`] match the specified path
/// [`Error::NoMatchesFound`] variant is returned.
///
/// ```
/// # use kanban_launcher_lib::gateway::{buffered::Config, buffered::ConfigBuffer};
/// # use kanban_launcher_lib::match_launcher_use_case::prelude::*;
/// # use pretty_assertions::assert_eq;
/// # use std::path::PathBuf;
/// # use vfs::{MemoryFS, FileSystem};
/// # let mock_file_system = MemoryFS::default();
/// # const UNRECOGNIZED_PROJECT_PATH: &str = "/other";
/// # mock_file_system.create_dir(UNRECOGNIZED_PROJECT_PATH).unwrap();
/// # let mut mock_gateway = Config(ConfigBuffer {
/// #     project_directory: None,
/// #     match_pattern: None,
/// # });
/// // GIVEN
/// let nonexistent_path = PathBuf::from("/nonexistent/path");
/// let unrecognized_project_path = PathBuf::from(UNRECOGNIZED_PROJECT_PATH);
///
/// // WHEN
/// let actual_nonexistent = match_launcher(MatchLauncherRequestModel {
///     file_system: &mock_file_system,
///     gateway: &mut mock_gateway,
///     project_path: &nonexistent_path,
/// });
///
/// let actual_unrecognized = match_launcher(MatchLauncherRequestModel {
///     file_system: &mock_file_system,
///     gateway: &mut mock_gateway,
///     project_path: &unrecognized_project_path,
/// });
///
/// // THEN
/// assert_eq!(
///     (actual_nonexistent, actual_unrecognized),
///     (
///         Err(MatchLauncherError::UnableToReadDirectory),
///         Err(MatchLauncherError::NoMatchesFound(unrecognized_project_path)),
///     )
/// );
/// ```
pub fn match_launcher<FS: FileSystem, G: ConfigTrait>(
    RequestModel {
        file_system,
        gateway,
        project_path,
    }: RequestModel<FS, G>,
) -> Result {
    let dir_vec = file_system
        .read_dir(project_path.to_str().unwrap_or_default())
        .map_err(|_| Error::UnableToReadDirectory)?
        .collect::<Vec<String>>();

    let match_pattern_vec =
        gateway.read_match_pattern_array().unwrap_or_default();

    for pattern in match_pattern_vec {
        if let Some(file_path) = pattern.match_dir(dir_vec.iter()) {
            let mut full_path = PathBuf::from(project_path);
            full_path.push(file_path.clone());
            return ResponseModel {
                instructions: pattern.launch_instructions,
                path_data: LaunchPathData {
                    project_directory: project_path.to_path_buf(),
                    file_name: file_path,
                    full_path,
                },
            }
            .into();
        }
    }

    Error::NoMatchesFound(project_path.to_path_buf()).into()
}
