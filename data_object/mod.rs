//! Data storage entities containing only validation code

mod launch_path_data;

pub use launch_path_data::LaunchPathData;
