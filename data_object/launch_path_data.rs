use std::path::PathBuf;

/// Path data for a specified project
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LaunchPathData {
    /// The directory that contains the project
    pub project_directory: PathBuf,

    /// The file that has been identified as the primary file in the project
    pub file_name: PathBuf,

    /// The full path ([`LaunchPathData::project_directory`]/[`LaunchPathData::file_name`])
    pub full_path: PathBuf,
}
