# Goals

- [ ] list -v (show ignored directories)
- [ ] Ignore directories using .gitignore syntax
- Config
  - [ ] Launcher pattern (if `Cargo.toml` then `code {DIR}`)
    - [ ] Append (assume last)
    - [ ] Insert @ index
    - [ ] Remove
    - [ ] Promote to Index
  - Directory
    - [ ] Add
    - [ ] Remove
    - [ ] Ignore
    - [ ] UnIgnore
- Project
  - Alias
    - List (default)
    - Add
    - Remove
  - Launcher
    - Display (default)
    - Set
    - Unset
    - Default (Launch using default, don't change)
- [ ] GitSummary (--date)
      `git log --oneline --decorate --all --graph --since="12AM"`
- [ ] Run git status and git pull on all projects (External Project
      `git_updater`)
- [ ] Update `quick_sync` to use `kanban_launcher` and `git_updater`
- [ ] Mark project complete
