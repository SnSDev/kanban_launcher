//! Pattern used to determine project launching instructions
//!
//! See [`LaunchPattern`]

use crate::entity::LaunchInstructions;
use crate::serde::Regex;
use std::path::PathBuf;

/// Pattern used to determine project launching instructions
#[derive(Debug, PartialEq, Eq, Clone, serde::Serialize, serde::Deserialize)]
pub struct LaunchPattern {
    /// Instructions to run if the project matches the pattern
    pub launch_instructions: LaunchInstructions,

    /// Pattern to run on files in project directory to determine match
    pub match_pattern: Regex,
}

impl LaunchPattern {
    /// Find files in specified directory that match pattern, return last
    ///
    /// This is used to determine which program is used to launch a
    /// project.
    ///
    /// Some projects have a single file to look for.  Eg:
    ///
    /// A Rust application will have a "Cargo.toml" file.  The pattern
    /// "Cargo\.toml" will return the Some variant if said file exists
    ///
    /// Some projects will have multiple versions of the same file stored with
    /// sequential or semantic version numbers.
    ///
    /// ```text
    /// - project_v_0_1_0.file
    /// - project_v_1_0_0.file
    /// - project_v_1_0_1.file
    /// ```
    ///
    /// In this example, the pattern ".*\.file" will return Some with the path
    /// to `project_v_1_0_1.file`
    ///
    /// # Example:
    ///
    /// ```
    /// # use kanban_launcher_lib::entity::{
    /// #     LaunchInstructionsToken as Token, LaunchPattern
    /// # };
    /// # use std::path::PathBuf;
    /// # use vfs::{FileSystem, MemoryFS};
    /// let mock_file_system = MemoryFS::default();
    /// mock_file_system.create_dir("/rust_project").unwrap();
    /// let mut writer = mock_file_system
    ///     .create_file("/rust_project/Cargo.toml")
    ///     .unwrap();
    /// writer.flush().unwrap();
    /// let mut writer = mock_file_system
    ///     .create_file("/rust_project/A.toml")
    ///     .unwrap();
    /// writer.flush().unwrap();
    /// let mut writer = mock_file_system
    ///     .create_file("/rust_project/B.toml")
    ///     .unwrap();
    /// writer.flush().unwrap();
    /// mock_file_system.create_dir("/other_project").unwrap();
    /// // GIVEN
    /// let launch_pattern = LaunchPattern {
    ///     launch_instructions: vec![
    ///         Token::Application("code".to_owned()),
    ///         Token::ProjectDirectory,
    ///     ]
    ///     .into(),
    ///     match_pattern: regex::Regex::new(r#".*\.toml"#).unwrap().into(),
    /// };
    ///
    /// // WHEN
    /// let actual_rust_project = launch_pattern
    ///     .match_dir(mock_file_system.read_dir("/rust_project").unwrap());
    /// let actual_other_project = launch_pattern
    ///     .match_dir(mock_file_system.read_dir("/other_project").unwrap());
    ///
    /// // THEN
    /// assert_eq!(
    ///     (actual_rust_project, actual_other_project),
    ///     (Some(PathBuf::from("Cargo.toml")), None)
    /// );
    /// ```
    pub fn match_dir<S: AsRef<str>, I: Iterator<Item = S>>(
        &self,
        dir: I,
    ) -> Option<PathBuf> {
        let mut matches = dir
            .map(|r| r.as_ref().to_owned())
            .filter(|file| self.match_pattern.is_match(file))
            .collect::<Vec<String>>();
        matches.sort();
        matches.last().map(PathBuf::from)
    }
}
