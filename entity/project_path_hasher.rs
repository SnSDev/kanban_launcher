use array_of_base::ArrayOfHex;
use pipe_trait::Pipe;
use sha2::{Digest, Sha256};
use std::path::Path;

/// Abbreviated hash of a project path
///
/// Generated by [`hash_project_path`]
pub type ProjectPathHash = ArrayOfHex<7>;

/// Generate [`ProjectPathHash`] from [`Path`]
///
/// ```
/// # use std::path::PathBuf;
/// # use kanban_launcher_lib::entity::{hash_project_path, ProjectPathHash};
/// // GIVEN
/// let path = PathBuf::from("/TestPath");
///
/// // WHEN
/// let actual = hash_project_path(&path);
///
/// // THEN
/// assert_eq!(
///     actual,
///     ProjectPathHash::try_new([2, 8, 11, 0, 15, 1, 5]).unwrap()
/// );
/// ```
pub fn hash_project_path(path_buff: &Path) -> ProjectPathHash {
    let mut hasher = Sha256::new();

    hasher.update(
        path_buff
            .to_str()
            .expect("1652605236 - Unreachable: String always present")
            .as_bytes(),
    );

    hasher
        .finalize()
        .iter()
        .copied()
        .pipe(|mut iter| {
            std::array::from_fn::<u8, 16, _>(|_| {
                iter.next().unwrap_or_default()
            })
        })
        .pipe(ArrayOfHex::from_u8_array)
        .pipe_borrow(ArrayOfHex::<32>::trim) // Trim to ArrayOfHex<7>
}
