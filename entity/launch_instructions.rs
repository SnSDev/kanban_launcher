//! Instructions to pass to operating system to launch project
//!
//! See [`LaunchInstructions`]

/// Instructions to pass to operating system to launch project
///
/// See: [`from_str`](#method.from_str) and [`fmt`](#method.fmt-1)
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LaunchInstructions(Vec<Token>);

impl From<Vec<Token>> for LaunchInstructions {
    fn from(value: Vec<Token>) -> Self {
        Self(value)
    }
}

impl From<LaunchInstructions> for Vec<Token> {
    fn from(value: LaunchInstructions) -> Self {
        value.0
    }
}

impl IntoIterator for LaunchInstructions {
    type Item = Token;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

/// A segment of [`LaunchInstructions`]
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Token {
    /// Path to program to launch
    Application(String),

    /// An argument e.g. "-v"
    Argument(String),

    /// A placeholder for the directory the project is in
    ProjectDirectory,

    /// A placeholder for the name of the primary file
    FileName,

    /// A placeholder for the full path to the primary file
    ///
    /// i.e. "[`Token::ProjectDirectory`]/[`Token::FileName`]"
    FullPath,
}

impl<'de> serde::Deserialize<'de> for LaunchInstructions {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        std::str::FromStr::from_str(&s)
            .map_err(|_| serde::de::Error::custom(""))
    }
}

impl serde::Serialize for LaunchInstructions {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl std::str::FromStr for LaunchInstructions {
    type Err = ();

    /// Parse the launch instructions into tokens
    ///
    /// # Example
    ///
    /// ```
    /// # use kanban_launcher_lib::entity::{
    /// #     LaunchInstructions, LaunchInstructionsToken as Token,
    /// # };
    /// // GIVEN
    /// let parsable = "app arg1 {DIR} arg2 {PATH} arg3 {FILE}";
    ///
    /// // WHEN
    /// let actual = parsable.parse::<LaunchInstructions>().unwrap();
    ///
    /// // THEN
    /// assert_eq!(
    ///     Vec::<Token>::from(actual),
    ///     vec![
    ///         Token::Application("app".to_owned()),
    ///         Token::Argument("arg1".to_owned()),
    ///         Token::ProjectDirectory,
    ///         Token::Argument("arg2".to_owned()),
    ///         Token::FullPath,
    ///         Token::Argument("arg3".to_owned()),
    ///         Token::FileName,
    ///     ]
    /// );
    /// ```
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(LaunchInstructions(
            s.split(' ')
                .enumerate()
                .map(|v| match v {
                    (0, app) => Token::Application(app.to_owned()),
                    (_, "{DIR}") => Token::ProjectDirectory,
                    (_, "{PATH}") => Token::FullPath,
                    (_, "{FILE}") => Token::FileName,
                    (_, arg) => Token::Argument(arg.to_owned()),
                })
                .collect::<Vec<Token>>(),
        ))
    }
}

impl std::fmt::Display for LaunchInstructions {
    /// Render token vector as string for storage
    ///
    /// # Example
    ///
    /// ```
    /// # use kanban_launcher_lib::entity::{
    /// #     LaunchInstructions, LaunchInstructionsToken as Token,
    /// # };
    /// // GIVEN
    /// let serializable = LaunchInstructions::from(vec![
    ///     Token::Application("app".to_owned()),
    ///     Token::Argument("arg1".to_owned()),
    ///     Token::ProjectDirectory,
    ///     Token::Argument("arg2".to_owned()),
    ///     Token::FullPath,
    ///     Token::Argument("arg3".to_owned()),
    ///     Token::FileName,
    /// ]);
    ///
    /// // WHEN
    /// let actual = serializable.to_string();
    ///
    /// // THEN
    /// assert_eq!(&actual, "app arg1 {DIR} arg2 {PATH} arg3 {FILE}");
    /// ```
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, v) in self.0.iter().enumerate() {
            if i != 0 {
                write!(f, " ")?;
            }

            write!(
                f,
                "{}",
                match v {
                    Token::Application(app) => app,
                    Token::ProjectDirectory => "{DIR}",
                    Token::FullPath => "{PATH}",
                    Token::FileName => "{FILE}",
                    Token::Argument(arg) => arg,
                }
            )?;
        }
        Ok(())
    }
}
