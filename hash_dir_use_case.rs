//! Return hash of the supplied directory
//!
//! See [`hash_dir`]

use crate::entity::hash_project_path;
pub use crate::hash_dir_use_case as prelude;
use array_of_base::ArrayOfBase;
use std::path::Path;

/// Response from [`hash_dir`]
pub struct RequestModel<'a> {
    /// The directory to hash
    pub directory: &'a Path,
}

impl<'a> From<&'a Path> for RequestModel<'a> {
    fn from(directory: &'a Path) -> Self {
        Self { directory }
    }
}

/// Parameters for [`hash_dir`]
#[derive(Debug, PartialEq)]
pub struct ResponseModel {
    /// The hash of [`RequestModel::directory`]
    pub hash: ArrayOfBase<7, 16>,
}

/// Return hash of the supplied directory
///
/// # Example
///
/// ```
/// # use std::{path::Path, path::PathBuf, str::FromStr};
/// # use kanban_launcher_lib::hash_dir_use_case::prelude::*;
/// // GIVEN
/// let path = PathBuf::from_str("/test/path").unwrap();
/// let request_model = RequestModel {
///     directory: &path,
/// };
///
/// // WHEN
/// let result = hash_dir(request_model);
///
/// // THEN
/// assert_eq!(
///     result.hash.to_string(),
///     "0c695c8".to_owned()
/// );
/// ```
#[must_use]
pub fn hash_dir(RequestModel { directory }: RequestModel) -> ResponseModel {
    ResponseModel {
        hash: hash_project_path(directory),
    }
}
