use dirs::home_dir;
use kanban_launcher_lib::gateway::{
    _trait::Config as ConfigTrait, buffered::ConfigBuffer,
};
use std::{fs::File, io::BufReader, path::PathBuf};

pub struct Config {
    pub config_path: Option<PathBuf>,
    pub load_error: Option<LoadError>,
    pub buffer: ConfigBuffer,
}

impl ConfigTrait for Config {
    fn read_project_directory_vec(&mut self) -> Option<Vec<PathBuf>> {
        self.buffer.project_directory.clone()
    }

    fn read_match_pattern_array(
        &mut self,
    ) -> Option<Vec<kanban_launcher_lib::entity::LaunchPattern>> {
        self.buffer.match_pattern.clone()
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum LoadError {
    DirIndeterminable,
    DirNotFound,
    FileNotFound,
    FileNotOpenable,
    FileNotParsable,
}

impl Config {
    fn err(load_error: LoadError, config_path: Option<PathBuf>) -> Self {
        Self {
            config_path,
            load_error: Some(load_error),
            buffer: load_default_config_buffer(),
        }
    }

    fn ok(contents: ConfigBuffer, config_path: PathBuf) -> Self {
        Self {
            config_path: Some(config_path),
            load_error: None,
            buffer: contents,
        }
    }

    pub fn load() -> Config {
        let Some(mut config_dir) =  dirs::config_dir() else {
            return Self::err(LoadError::DirIndeterminable, None);
    };

        config_dir.push("kanban_launcher");
        let mut config_path = config_dir.clone();
        config_path.push("config.json");

        if !config_dir.exists() {
            return Self::err(LoadError::DirNotFound, Some(config_path));
        }

        if !config_path.exists() {
            return Self::err(LoadError::FileNotFound, Some(config_path));
        }

        let Ok(file) = File::open(config_path.clone()) else {
        return  Self::err(LoadError::FileNotOpenable, Some(config_path));
    };

        let reader = BufReader::new(file);

        let config = match serde_json::from_reader(reader) {
            Ok(v) => v,
            Err(_e) => {
                // eprintln!("{e:?}");
                return Self::err(
                    LoadError::FileNotParsable,
                    Some(config_path),
                );
            }
        };

        Self::ok(config, config_path)
    }
}

fn load_default_config_buffer() -> ConfigBuffer {
    ConfigBuffer {
        project_directory: Some(vec![{
            let mut home_dir = home_dir().unwrap_or(PathBuf::from("/"));
            home_dir.push("Projects");
            home_dir
        }]),
        match_pattern: None,
    }
}
