use kanban_launcher_lib::{
    gateway::_trait::Config, list_projects_use_case::prelude::*,
};
use vfs::FileSystem;

pub fn run_list_projects_use_case<G: Config, FS: FileSystem>(
    gateway: &mut G,
    file_system: &FS,
) -> ListProjectsResponseIterator {
    match list_projects(ListProjectsRequestModel {
        file_system,
        gateway,
    }) {
        Ok(ListProjectsResponseModel { reference_vec }) => {
            ListProjectsResponseIterator {
                iter: reference_vec.into_iter(),
            }
        }
        Err(e) => {
            eprintln!("{e:?}");
            ListProjectsResponseIterator {
                iter: Vec::new().into_iter(),
            }
        }
    }
}

pub struct ListProjectsResponseIterator {
    iter: std::vec::IntoIter<ProjectReference>,
}

impl Iterator for ListProjectsResponseIterator {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(format_project_reference)
    }
}

fn format_project_reference(
    ProjectReference { file_path, hash }: ProjectReference,
) -> String {
    format!("{hash} - {}", file_path.as_os_str().to_str().unwrap_or(""))
}
