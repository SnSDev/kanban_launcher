use colored::Colorize;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum SettingState {
    Default,
    Empty,
    Set,
    Header,
    Line,
}

struct ConfigLine {
    pub setting: String,
    pub state: SettingState,
    pub value: String,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Default)]
struct ColumnFormatting {
    pub setting_width: usize,
    pub value_width: usize,
}

impl ConfigLine {
    pub fn line() -> Self {
        Self {
            setting: String::new(),
            state: SettingState::Line,
            value: String::new(),
        }
    }

    pub fn print_line(&self, formatting: ColumnFormatting) {
        let formatted_value = match self.state {
            SettingState::Set => self.value.green().bold(),
            SettingState::Empty => self.value.black().italic(),
            SettingState::Header => self.value.yellow().bold(),
            _ => self.value.white(),
        };

        let formatted_setting = match self.state {
            SettingState::Set => self.setting.blue().bold(),
            SettingState::Header => self.setting.yellow().bold(),
            _ => self.setting.blue(),
        };

        if matches!(self.state, SettingState::Line) {
            println!(
                "| {:-<sw$} | {:-<vw$} |",
                ":",
                ":",
                sw = formatting.setting_width,
                vw = formatting.value_width,
            );
        } else {
            println!(
                "| {formatted_setting:sw$} | {formatted_value:vw$} |",
                sw = formatting.setting_width,
                vw = formatting.value_width,
            );
        }
    }

    pub fn update_formatting(&self, formatting: &mut ColumnFormatting) {
        formatting.setting_width =
            self.setting.len().max(formatting.setting_width);
        formatting.value_width = self.value.len().max(formatting.value_width);
    }
}

pub struct ConfigTable {
    rows: Vec<ConfigLine>,
}

impl Default for ConfigTable {
    fn default() -> Self {
        Self {
            rows: vec![
                ConfigLine {
                    setting: "Setting".to_owned(),
                    state: SettingState::Header,
                    value: "Value".to_owned(),
                },
                ConfigLine::line(),
            ],
        }
    }
}

impl ConfigTable {
    pub fn render(&self) {
        let mut formatting = ColumnFormatting::default();

        self.rows
            .iter()
            .for_each(|row| row.update_formatting(&mut formatting));
        self.rows.iter().for_each(|row| row.print_line(formatting));
    }

    pub fn insert_set<S: AsRef<str>>(
        &mut self,
        key: &str,
        value_vec: &[S],
    ) -> &mut Self {
        if value_vec.is_empty() {
            return self.insert_empty(key);
        }

        self.insert_generic(key, value_vec, SettingState::Set)
    }

    pub fn insert_empty(&mut self, key: &str) -> &mut Self {
        self.rows.push(ConfigLine {
            setting: key.to_owned(),
            state: SettingState::Empty,
            value: "(Not Set)".to_owned(),
        });

        self
    }

    fn insert_generic<S: AsRef<str>>(
        &mut self,
        key: &str,
        value_vec: &[S],
        state: SettingState,
    ) -> &mut Self {
        value_vec.iter().enumerate().for_each(|(i, v)| {
            self.rows.push(ConfigLine {
                setting: (i == 0).then_some(key).unwrap_or_default().to_owned(),
                state,
                value: v.as_ref().to_owned(),
            });
        });

        self
    }

    pub fn insert_default<S: AsRef<str>>(
        &mut self,
        key: &str,
        value_vec: &[S],
    ) -> &mut Self {
        if value_vec.is_empty() {
            return self.insert_empty(key);
        }

        self.insert_generic(key, value_vec, SettingState::Default)
    }
}

#[test]
fn feature1670523105() {
    ConfigTable::default()
        .insert_default("Default", &["Value1", "Value2", "Value3"])
        .insert_set("Set", &["Value4", "Value5", "Value6"])
        .insert_default("EmptyD", &Vec::<String>::new())
        .insert_set("EmptyS", &Vec::<String>::new())
        .render();
}
