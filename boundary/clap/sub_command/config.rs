use crate::{
    entity::ConfigTable,
    gateway::json::{Config as JsonConfig, ConfigLoadError},
};
use clap::{ArgMatches, Command};
use colored::Colorize;
use kanban_launcher_lib::gateway::_trait::Config as _;

const SUB_COMMAND_NAME: &str = "config";

pub trait Config: Sized {
    fn subcommand_config(self) -> Self {
        unreachable!("1669944036")
    }

    fn matches_config(self, _gateway: &mut JsonConfig) -> Self {
        unreachable!("1669943998")
    }
}

impl Config for Command {
    fn subcommand_config(self) -> Self {
        self.subcommand(
            Command::new(SUB_COMMAND_NAME).about("Output / Update config file"),
        )
    }
}

impl Config for ArgMatches {
    fn matches_config(self, gateway: &mut JsonConfig) -> Self {
        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        println!();

        println!(
            "{}: {}",
            "Config Path".bold(),
            if let Some(path) = gateway.config_path.clone() {
                path.to_str().unwrap_or_default().green()
            } else {
                "(Unable to determine)".red().italic()
            }
        );

        if let Some(err) = gateway.load_error {
            println!(
                "{}: {}",
                "Load Error".bold(),
                match err {
                    ConfigLoadError::DirIndeterminable => "N/A",
                    ConfigLoadError::DirNotFound => "Directory not found",
                    ConfigLoadError::FileNotFound => "File not found",
                    ConfigLoadError::FileNotOpenable => "Unable to open file",
                    ConfigLoadError::FileNotParsable => "Unable to parse file",
                }
                .red()
                .italic()
            );
        }

        println!();

        let mut table = ConfigTable::default();

        let project_directory_vec = gateway.read_project_directory_vec();

        (if gateway.load_error.is_none() && project_directory_vec.is_some() {
            ConfigTable::insert_set
        } else {
            ConfigTable::insert_default
        })(
            &mut table,
            "project_directory",
            &project_directory_vec
                .unwrap_or_default()
                .iter()
                .map(|p| p.to_str().unwrap_or_default())
                .collect::<Vec<&str>>(),
        );

        let match_pattern_vec = gateway.read_match_pattern_array();

        (if gateway.load_error.is_none() && match_pattern_vec.is_some() {
            ConfigTable::insert_set
        } else {
            ConfigTable::insert_default
        })(
            &mut table,
            "match_pattern",
            &match_pattern_vec
                .unwrap_or_default()
                .iter()
                .map(|p| {
                    format!(
                        "{:?} - {:?}",
                        p.match_pattern.as_str(),
                        p.launch_instructions.to_string()
                    )
                })
                .collect::<Vec<String>>(),
        );

        table.render();

        println!();

        self
    }
}
