use std::path::Path;

use clap::ArgMatches;
use kanban_launcher_lib::hash_dir_use_case::prelude::*;

pub trait Default {
    fn matches_default(self, current_dir: &Path) -> Self;
}

impl Default for ArgMatches {
    fn matches_default(self, current_dir: &Path) -> Self {
        if self.subcommand().is_none() {
            println!("{}", hash_dir(current_dir.into()).hash);
        }
        self
    }
}
