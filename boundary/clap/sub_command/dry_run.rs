use clap::{ArgMatches, Command};
use colored::Colorize;
use kanban_launcher_lib::{
    entity::LaunchInstructionsToken, gateway::_trait::Config,
    match_launcher_use_case::prelude::*,
};
use std::path::Path;
use vfs::FileSystem;

const SUB_COMMAND_NAME: &str = "dry_run";

pub trait DryRun: Sized {
    fn subcommand_dry_run(self) -> Self {
        unreachable!("1670614218")
    }

    fn matches_dry_run<G: Config, FS: FileSystem>(
        self,
        _gateway: &mut G,
        _file_system: &FS,
        _current_dir: &Path,
    ) -> Self {
        unreachable!("1670614242")
    }
}

impl DryRun for Command {
    fn subcommand_dry_run(self) -> Self {
        self.subcommand(Command::new(SUB_COMMAND_NAME).about(
            "Output the command that will be run if the project in current \
             directory was run",
        ))
    }
}

impl DryRun for ArgMatches {
    fn matches_dry_run<G: Config, FS: FileSystem>(
        self,
        gateway: &mut G,
        file_system: &FS,
        current_dir: &Path,
    ) -> Self {
        use LaunchInstructionsToken as Token;

        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        let request_model = MatchLauncherRequestModel {
            file_system,
            gateway,
            project_path: current_dir,
        };

        match match_launcher(request_model) {
            Err(MatchLauncherError::NoMatchesFound(path)) => {
                println!(
                    "{}: \"{}\"",
                    "File Browser (Fallback)".red().bold(),
                    path.to_str().unwrap_or_default()
                );
            }
            Ok(MatchLauncherResponseModel {
                instructions,
                path_data,
            }) => {
                for (i, token) in
                    Vec::<Token>::from(instructions).iter().enumerate()
                {
                    if i != 0 {
                        print!(" ");
                    }
                    print!(
                        "{}",
                        match token {
                            Token::Application(app) => app.green().bold(),
                            Token::Argument(arg) => arg.white(),
                            Token::FileName => format!(
                                "\"{}\"",
                                path_data
                                    .file_name
                                    .to_str()
                                    .unwrap_or_default()
                            )
                            .yellow(),
                            Token::FullPath => format!(
                                "\"{}\"",
                                path_data
                                    .full_path
                                    .to_str()
                                    .unwrap_or_default()
                            )
                            .yellow(),
                            Token::ProjectDirectory => format!(
                                "\"{}\"",
                                path_data
                                    .project_directory
                                    .to_str()
                                    .unwrap_or_default()
                            )
                            .yellow(),
                        }
                    );
                }
                println!();
            }
            Err(e) => eprintln!("{e:?}"),
        }

        self
    }
}
