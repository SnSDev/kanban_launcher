use crate::entity::run_list_projects_use_case;
use clap::{ArgMatches, Command};
use kanban_launcher_lib::gateway::_trait::Config;
use std_io_iterators::prelude::*;
use vfs::FileSystem;

const SUB_COMMAND_NAME: &str = "list";

pub trait List {
    fn subcommand_list(self) -> Self;
    fn matches_list<G: Config, FS: FileSystem>(
        self,
        gateway: &mut G,
        file_system: &FS,
    ) -> Self;
}

impl List for Command {
    fn subcommand_list(self) -> Self {
        self.subcommand(
            Command::new(SUB_COMMAND_NAME).about(
                "List all projects in project folders specified in config",
            ),
        )
    }

    fn matches_list<G: Config, FS: FileSystem>(
        self,
        _gateway: &mut G,
        _file_system: &FS,
    ) -> Self {
        unreachable!("1670374441")
    }
}

impl List for ArgMatches {
    fn subcommand_list(self) -> Self {
        unreachable!("1670374445")
    }

    fn matches_list<G: Config, FS: FileSystem>(
        self,
        gateway: &mut G,
        file_system: &FS,
    ) -> Self {
        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        let _ = run_list_projects_use_case(gateway, file_system).pipe_out();

        self
    }
}
