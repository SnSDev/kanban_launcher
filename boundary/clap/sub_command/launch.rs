use crate::entity::run_list_projects_use_case;
use clap::{ArgMatches, Command};
use kanban_launcher_lib::data_object::LaunchPathData;
use kanban_launcher_lib::entity::{
    LaunchInstructions, LaunchInstructionsToken as Token,
};
use kanban_launcher_lib::gateway::_trait::Config;
use kanban_launcher_lib::match_launcher_use_case::prelude::*;
use std::io::Write;
use std::path::PathBuf;
use std::process::{self, Stdio};
use vfs::FileSystem;

const SUB_COMMAND_NAME: &str = "launch";

pub trait Launch: Sized {
    fn subcommand_launch(self) -> Self {
        unreachable!("1670452140")
    }

    fn matches_launch<G: Config, FS: FileSystem>(
        self,
        _gateway: &mut G,
        _file_system: &FS,
    ) -> Self {
        unreachable!("1670452132")
    }
}

impl Launch for Command {
    fn subcommand_launch(self) -> Self {
        self.subcommand(Command::new(SUB_COMMAND_NAME).about(
            "Prompt user to select project from list of projects, open the \
             project",
        ))
    }
}

impl Launch for ArgMatches {
    fn matches_launch<G: Config, FS: FileSystem>(
        self,
        gateway: &mut G,
        file_system: &FS,
    ) -> Self {
        let Some(_matches) = self.subcommand_matches(SUB_COMMAND_NAME) else {
            return self;
        };

        let list_string = run_list_projects_use_case(gateway, file_system)
            .collect::<Vec<String>>()
            .join("\n");

        let mut dmenu_launcher = process::Command::new("dmenu_launcher")
            .arg("--stdout")
            .arg("--col=2")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .expect("1670454622 - Unable to spawn dmenu_launcher");

        let mut dmenu_launcher_stdin = dmenu_launcher
            .stdin
            .take()
            .expect("1669429260 - Unable to open dmenu_launcher's STDIN");

        dmenu_launcher_stdin
            .write_all(list_string.as_bytes())
            .expect("1670454673 - Unable to pipe out");

        // Close stdin to finish and avoid indefinite blocking
        drop(dmenu_launcher_stdin);

        let dmenu_launcher_output = dmenu_launcher
            .wait_with_output()
            .expect("1670454716 - Waiting with output failure");

        let dmenu_launcher_selection = if dmenu_launcher_output.status.success()
        {
            String::from_utf8(dmenu_launcher_output.stdout).expect(
                "1670454744 - Unable to retrieve output from dmenu_launcher",
            )
        } else {
            eprintln!("dmenu_launcher returned error state");
            return self;
        };

        if dmenu_launcher_selection.is_empty() {
            return self;
        }

        let request_model = MatchLauncherRequestModel {
            file_system,
            gateway,
            project_path: &PathBuf::from(dmenu_launcher_selection.trim()),
        };

        match match_launcher(request_model) {
            Ok(MatchLauncherResponseModel {
                instructions,
                path_data,
            }) => launch_app(instructions, path_data),
            Err(MatchLauncherError::NoMatchesFound(path)) => open::that(path)
                .expect("1670455154 - Unable to open supplied path"),
            Err(e) => eprintln!("{e:?}"),
        }

        self
    }
}

fn launch_app(
    instructions: LaunchInstructions,
    LaunchPathData {
        project_directory,
        file_name,
        full_path,
    }: LaunchPathData,
) {
    let mut iter = instructions.into_iter();

    let Some(Token::Application(app)) = iter.next() else {
        unreachable!("1670697900 - Malformed launch instructions");
    };

    let mut app = process::Command::new(app);

    for arg in iter {
        match arg {
            Token::Application(_) => {
                unreachable!("1670697988 - Multiple app tokens")
            }
            Token::Argument(arg) => {
                app.arg(arg);
            }
            Token::FileName => {
                app.arg(file_name.as_os_str());
            }
            Token::FullPath => {
                app.arg(full_path.as_os_str());
            }
            Token::ProjectDirectory => {
                app.arg(project_directory.as_os_str());
            }
        }
    }

    app.spawn().expect("1670698324 - Unable to launch program");
}
