//! Prompt all current projects in system, Launch selected project
//!
//! See [`kanban_launcher_lib`]

#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

mod entity;
mod gateway;
mod sub_command;

use clap::{crate_authors, crate_description, crate_name, Command};
use gateway::json::Config as JsonConfig;
use sub_command::{
    Config as _, Default as _, DryRun as _, Launch as _, List as _,
};
use vfs::PhysicalFS;

fn main() {
    let app = Command::new(crate_name!())
        .author(crate_authors!())
        .about(crate_description!())
        // .color(clap::ColorChoice::Always)
        // .disable_colored_help(false)
        .subcommand_config()
        .subcommand_list()
        .subcommand_launch()
        .subcommand_dry_run()
        .version(env!("GIT_COMMIT_DESCRIBE"));

    let mut config_gateway = JsonConfig::load();
    let file_system = PhysicalFS::new("/");
    let current_dir = std::env::current_dir()
        .expect("1652606287 - Unreachable: Issue loading current directory");

    app.get_matches()
        .matches_config(&mut config_gateway)
        .matches_list(&mut config_gateway, &file_system)
        .matches_launch(&mut config_gateway, &file_system)
        .matches_dry_run(&mut config_gateway, &file_system, &current_dir)
        .matches_default(&current_dir);
}
