# Version History

- [0.1.0](#010) - Proof of concept, not committed
- [0.1.1](#011) - Rename / move project locally, Initial commit, Not compiled,
  not committed
- [0.2.0](#020) - Update hashing algorithm, Rust edition, project architecture

## 0.1.0

_Note: All changes done locally and outside the repository_

- Add use case: hash_dir\
  When `kanban` is run in the command line, the `$PWD` (current directory) path is
  hashed using `SHA1` and truncated at 7 chars, output to `STDOUT`

## 0.1.1

_Note: All changes done locally and outside the repository_

_Note: Project not compiled_

- Move project to different project folder
- Rename project (binary and library)

## 0.2.0

- Update hashing algorithm (`SHA1` to `SHA256`)
- Add clippy::pedantic
- Create workspace, Move binary to boundary/clap
