//! Wrapper implementing serialization / deserialization for external data structures

pub mod regex;

pub use self::regex::Regex;
