//! Wrapper for [`regex::Regex`] implementing serialize / deserialize
//!
//! See [`Regex`]

/// Wrapper for [`regex::Regex`] implementing serialize / deserialize
///
/// Uses
/// - `regex::Regex::new` to deserialize / parse
/// - `regex::Regex::fmt` to serialize / format
#[derive(Debug, Clone)]
pub struct Regex(regex::Regex);

impl serde::Serialize for Regex {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.to_string())
    }
}

impl<'de> serde::Deserialize<'de> for Regex {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        std::str::FromStr::from_str(&s).map_err(serde::de::Error::custom)
    }
}

impl std::ops::Deref for Regex {
    type Target = regex::Regex;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl PartialEq for Regex {
    fn eq(&self, other: &Self) -> bool {
        self.0.to_string() == other.0.to_string()
    }
}

impl Eq for Regex {
    fn assert_receiver_is_total_eq(&self) {}
}

impl std::fmt::Display for Regex {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl std::str::FromStr for Regex {
    type Err = regex::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        regex::Regex::new(s).map(Regex::from)
    }
}

impl From<regex::Regex> for Regex {
    fn from(value: regex::Regex) -> Self {
        Regex(value)
    }
}

impl From<Regex> for regex::Regex {
    fn from(value: Regex) -> Self {
        value.0
    }
}
